using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is an example of how you can keep related data together in a tiny little container that is configurable from within the Unity editor.
/// Adding the [System.Serializable] attribute is all that is needed to make a tiny little class serializable to the editor interface!
/// </summary>
[System.Serializable]
public class AudioInfo
{
	public AudioClip soundClip;

	[RangeAttribute(0,1)]
	public float volume = 1f;
}