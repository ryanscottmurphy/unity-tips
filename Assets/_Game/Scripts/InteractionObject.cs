﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script shows how you can call out to your global singleton objects and give them the context that they need to execute inside your scene.
/// </summary>
public class InteractionObject : MonoBehaviour
{
	[SerializeField] InteractionScript context;

	void OnMouseOver()
	{
		WorldController.Instance.InformationText.text = context.mouseOverText;
	}

	void OnMouseDown()
	{
		WorldController.Instance.PlaySpeech(context.interactionText);
		WorldController.Instance.PlayAudio(context.audio);
	}
}