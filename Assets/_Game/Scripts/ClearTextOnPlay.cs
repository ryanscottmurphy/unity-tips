﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This is a type of script that I use for initializing objects in my scene.
/// It deletes itself after it initializes things, which I think is useful.
/// </summary>
public class ClearTextOnPlay : MonoBehaviour
{
	void Awake()
	{
		GetComponent<Text>().text = "";
		Destroy(this);
	}
}