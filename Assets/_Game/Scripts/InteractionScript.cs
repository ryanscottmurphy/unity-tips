﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is an example of a scriptable object. To put it in the most simple terms possible, this is a trimmed down MonoBehaviour which is
/// far better suited to holding data for your game. It only receives Awake(), OnDestroy(), OnEnable() and OnDisable() messages though.
/// 
/// Deriving from the ScriptableObject class is all you need to do. Adding the [CreateAssetMenu] attribute will allow you to create an asset
/// from the submenu under Right Click > Create (you should find InteractionScript in this project.
/// </summary>
[CreateAssetMenu]
public class InteractionScript : ScriptableObject
{
	public string interactionText;
	public string mouseOverText;
	public AudioInfo audio;
}