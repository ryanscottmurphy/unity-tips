﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldController : MonoBehaviour
{
	public Text InformationText;
	public Text PlayerSpeechBubbleText;

	[SerializeField] Image speechBubbleBackground;
	[SerializeField] AudioSource audioSource;

	/// <summary>
	/// This is an example of a very lazy little Singleton pattern.
	/// The point of the Singleton pattern is to keep one globally accessible Instance variable available to all other objects in the game.
	/// For things that are part of the "World" or super commonly used objects like "Player" or something (assuming there's only one).
	/// It's considered bad practice to use it a lot - but who cares if it makes what you're doing easy!
	/// 
	/// For small games, it's perfectly acceptable (frankly, I've seen the same pattern used promiscuously in rather large games).
	/// Here are some links to GitHub gists that you can copy the template:
	/// https://gist.github.com/ryanscottmurphy/47f0e3d93c5a89b244b154b16d60678a
	/// https://gist.github.com/ryanscottmurphy/393c147e73c06731672e1bc0018f7533
	/// 
	/// Here's a more information and a generic version that you can use everywhere:
	/// http://wiki.unity3d.com/index.php/Singleton
	/// </summary>
	#region SINGLETON PATTERN
	private static WorldController sharedInstance = null;
	public static WorldController Instance {
		get
		{
			if (sharedInstance == null)
			{
				sharedInstance = FindObjectOfType(typeof(WorldController)) as WorldController;
				if (sharedInstance == null)
				{
					Debug.LogError("Could not locate a Type object. You have to have exactly one WorldController in the scene.");
					return null;
				}
			}
			return sharedInstance;
		}
	}

	void Awake()
	{
		if (sharedInstance != null && sharedInstance != this)
		{
			DestroyImmediate(this);
		}
		else
		{
			sharedInstance = this;
		}
	}
	#endregion

	void Start()
	{
		speechBubbleBackground.enabled = false;
	}

	public void PlaySpeech(string text)
	{
		StopAllCoroutines();
		StartCoroutine(PlaySpeechCoroutine(text));
	}

	IEnumerator PlaySpeechCoroutine(string text)
	{
		speechBubbleBackground.enabled = true;
		yield return new WaitForSeconds(0.2f);
		string currentText = "";
		foreach (char c in text)
		{
			currentText += c;
			yield return new WaitForSeconds(0.05f);
			PlayerSpeechBubbleText.text = currentText;
		}
		yield return new WaitForSeconds(4f);
		speechBubbleBackground.enabled = false;
		PlayerSpeechBubbleText.text = "";
	}

	public void PlayAudio(AudioInfo audio)
	{
		audioSource.PlayOneShot(audio.soundClip, audio.volume);
	}
}